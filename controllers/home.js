/// CONTROLLERS
(function () {
    'use strict';

    angular
        .module('weatherApp')
        .controller('HomeController', HomeController);

    HomeController.inject = ['cityService', '$location'];

    function HomeController(cityService, $location) {
        var vm = this;
        vm.city = cityService.city;
        console.log('City in Home Controller: ' + vm.city);
        activate();

        ////////////////

        function activate() { }

        vm.submit = function () {
            cityService.city = vm.city;
            $location.path('/forecast')
        }
    }
})();
