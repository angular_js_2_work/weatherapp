(function() {
'use strict';

    // Usage:
    //
    // Creates:
    //

    angular
        .module('weatherApp')
        .component('myReport', {
            templateUrl: 'directives/myReport.htm',
            controller: ControllerController,
            bindings: {
                result: '=',
            },
        });

    ControllerController.inject = [];
    function ControllerController() {
        var vm = this;
        console.log(vm);


        ////////////////

        vm.onInit = function() {
        };
        vm.onChanges = function(changesObj) { };
        vm.onDestory = function() { };
    }
})();

///can also be component with bindings : { data: '<'}
