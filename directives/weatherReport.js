(function() {
     'use strict';

     angular
         .module('weatherApp')
         .directive('weatherReport', WeatherReport);

     WeatherReport.inject = [];
     function WeatherReport() {
         // Usage:
         //
         // Creates:
         //
         var directive = {
             templateUrl: 'directives/weatherReport.htm',

             restrict: 'E',
             replace: true,
             scope: {
                 weatherDay: '=',
                 convertToStandard: '&',
                 convertToDate: '&',
                 dateFormat: '@'
             }
         };
         return directive;


     }
 })();
