(function () {
    'use strict';

    angular
        .module('weatherApp')
        .controller('ForecastController', ForecastController);

    ForecastController.inject = ['cityService',"$routeParams",'weatherService'];
    function ForecastController(cityService, $routeParams, weatherService) {
        var vm = this;

        
        activate();

        ////////////////

        function activate() { }

        
        vm.city = cityService.city;
        vm.days = $routeParams.days || '2';
        console.log(vm.city);


        vm.weatherResult = weatherService.getWeather(vm.city, vm.days)
        vm.details = [];
        console.log(vm.weatherResult);
        vm.convertToCelsius = function(degreesKelvin) {
            return  Math.round(degreesKelvin - 273.15) ;
        }

        vm.convertToDate = function(dt) {
            return new Date(dt * 1000);
        }

        vm.myFilter = function (item) {
            var results = [];
            angular.forEach(item, function(itrr) {
                const res =  {
                    dt: itrr.dt,
                    temp : vm.convertToCelsius(itrr.temp.day)
                };
                results.push(res);
            })
            return results;
        };
    }
})();
