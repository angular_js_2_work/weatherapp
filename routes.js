/// usage of sceDelegateProvider is needed for allowing http requests wiht angularjs 1.6
/// ROUTES
(function () {
    'use strict';

    angular
        .module('weatherApp')
        .config(configurator)

    configurator.$inject = ["$routeProvider", "$sceDelegateProvider", "$locationProvider"];
    /** @ngInject */
    function configurator($routeProvider, $sceDelegateProvider, $locationProvider) {
        $sceDelegateProvider.resourceUrlWhitelist([
            // Allow same origin resource loads.
            'self',
            // Allow loading from our assets domain. **.
            'http://api.openweathermap.org/**'
        ]);
        $routeProvider
            .when('/', {
                templateUrl: 'pages/home.htm',
                controller: 'HomeController',
                controllerAs: 'vm'
            })
            .when('/forecast', {
                templateUrl: 'pages/forecast.htm',
                controller: 'ForecastController',
                controllerAs: 'vm'
            })
            .when('/forecast/:days', {
                templateUrl: 'pages/forecast.htm',
                controller: 'ForecastController',
                controllerAs: 'vm'
            });

    }

})();
