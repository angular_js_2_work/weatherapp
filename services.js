/// SERVICES
(function () {
    'use strict';

    angular
        .module('weatherApp')
        .factory('cityService', cityService);

    cityService.$inject = [];



    function cityService() {

        this.city = "New York, NY";

        return this;

    }

})();


(function () {
    'use strict';

    angular
        .module('weatherApp')
        .factory('weatherService', weatherService);

    weatherService.$inject = ["$resource"];



    function weatherService($resource) {
        this.getWeather = getWeather;





        var weatherApi = $resource("http://api.openweathermap.org/data/2.5/forecast/daily",

            { get: { method: "JSONP"} }
        );

        const APP_ID = '5804a87311c2208bc860cc94db0caa32';

        function getWeather(city, days) {
            console.log(weatherApi);
           return weatherApi.get({ q: city, cnt : days, appid :  APP_ID});
        }
        // public API
        return {
            getWeather : getWeather
        };
    }

})();

